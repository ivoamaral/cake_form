<nav class="col-md-2 columns" id="actions-sidebar">
    <ul class="nav nav-pills nav-stacked">
        <li class="active"><a><?= __('Actions') ?></a></li>
        <li><?= $this->Html->link(__('List {0}', 'Companies'), ['action' => 'index']) ?></li>
    </ul>
</nav>
<div class="companies form col-md-10 columns content">
    <?= $this->Form->create($company, ['type' => 'file']) ?>
    <fieldset>
        <legend><?= 'Add Company' ?></legend>
        <?php
            echo $this->Form->input('name');
            echo $this->Form->input('city');
            echo $this->Form->input('established_date');
            echo $this->Form->input('logo', ['type' => 'file']);
            echo $this->Form->input('location', ['id' => 'location', 'type' => 'hidden']);
        ?>
    </fieldset>
    <div id="map_canvas"></div>
    <?= $this->Form->button(__('Submit')) ?>
    <?= $this->Form->end() ?>
</div>

<style type="text/css">
  #map_canvas {
    width: 680px;
    height: 300px;
}
</style>

<script src="http://maps.googleapis.com/maps/api/js?key=AIzaSyClk2L52TSCeplOCAbaCTwPe5X0qz_B0Cw"></script>

<script type="text/javascript">
    var map = new google.maps.Map(document.getElementById('map_canvas'), {
        zoom: 5,
        center: new google.maps.LatLng(43.661406, -79.378936),
        mapTypeId: google.maps.MapTypeId.ROADMAP
    });

    var myMarker = new google.maps.Marker({
        position: new google.maps.LatLng(43.661406, -79.378936),
        draggable: true
    });

    google.maps.event.addListener(myMarker, 'dragend', function (evt) {
        var lat_long = evt.latLng.lat() + "," + evt.latLng.lng();
        document.getElementById('location').value = lat_long;
    });
    map.setCenter(myMarker.position);
    myMarker.setMap(map);
</script>