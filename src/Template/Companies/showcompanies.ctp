<div class="row">
<nav class="col-md-2" id="actions-sidebar">
    <ul class="nav nav-pills nav-stacked">
        <li><a><?= __('Actions') ?></a></li>
        <li><?= $this->Html->link(__('New {0}', ['Company']), ['action' => 'add']) ?></li>
        <li class="active"><?= $this->Html->link(__('Show Companies (Map)'), ['action' => 'showcompanies']) ?></li>
    </ul>
</nav>
<div class="companies index col-md-10 columns content">
    <h3>Companies</h3>
    <div id="map_canvas"></div>
</div>
</div>

<style type="text/css">
  #map_canvas {
    width: 680px;
    height: 300px;
}
</style>

<script src="https://developers.google.com/maps/documentation/javascript/examples/markerclusterer/markerclusterer.js"></script>
<script src="http://maps.googleapis.com/maps/api/js?key=AIzaSyClk2L52TSCeplOCAbaCTwPe5X0qz_B0Cw"></script>

<script type="text/javascript">
    function initialize() {
        var companyData = <?php echo $companyData; ?>;

        var center = new google.maps.LatLng(43.661406, -79.378936);

        var map = new google.maps.Map(document.getElementById('map_canvas'), {
            zoom: 5,
            center: center,
            mapTypeId: google.maps.MapTypeId.ROADMAP
        });

        var markers = [];
        for (var i = 0; i < companyData.length; i++) {
            var latLng = new google.maps.LatLng(companyData[i][1],companyData[i][2]);
            var marker = new google.maps.Marker({
                position: latLng
            });
            markers.push(marker);
        }
        var markerCluster = new MarkerClusterer(map, markers, {imagePath: 'https://developers.google.com/maps/documentation/javascript/examples/markerclusterer/m'});
    }    

    google.maps.event.addDomListener(window, 'load', initialize);
</script>