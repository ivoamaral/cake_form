<nav class="col-lg-2 col-md-3">
    <ul class="nav nav-pills nav-stacked">
        <li class="active"><a href=""><?= __('Actions') ?></a></li>
        <li><?= $this->Html->link(__('Edit {0}', ['Company']), ['action' => 'edit', $company->id]) ?> </li>
        <li><?= $this->Form->postLink(__('Delete {0}', ['Company']), ['action' => 'delete', $company->id], ['confirm' => __('Are you sure you want to delete # {0}?', $company->id)]) ?> </li>
        <li><?= $this->Html->link(__('List {0}', ['Companies']), ['action' => 'index']) ?> </li>
        <li><?= $this->Html->link(__('New {0}', ['Company']), ['action' => 'add']) ?> </li>
    </ul>
</nav>
<div class="companies view col-lg-10 col-md-9">
    <h3><?= h($company->name) ?></h3>
    <table class="table table-striped table-hover">
        <tr>
            <th>Name</th>
            <td><?= h($company->name) ?></td>
        </tr>
        <tr>
            <th>City</th>
            <td><?= h($company->city) ?></td>
        </tr>
        <tr>
            <th>Logo</th>
            <td><?= h($company->logo) ?></td>
        </tr>
        <tr>
            <th>Location</th>
            <td><?= h($company->location) ?></td>
        </tr>
        <tr>
            <th>'Id</th>
            <td><?= $this->Number->format($company->id) ?></td>
        </tr>
        <tr>
            <th>Established Date</th>
            <td><?= h($company->established_date) ?></tr>
        </tr>
        <tr>
            <th>Created</th>
            <td><?= h($company->created) ?></tr>
        </tr>
        <tr>
            <th>Modified</th>
            <td><?= h($company->modified) ?></tr>
        </tr>
    </table>
</div>
